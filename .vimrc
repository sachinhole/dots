" my vimrc
set encoding=utf8		" Set utf8 as standard encoding and en_US as the standard language
set number
syntax on
set background=dark
hi CursorLine	cterm=bold 
set cursorline
set wildmenu			" Turn on the Wild menu
set ruler				" Always show current position
set ignorecase  		" Ignore case when searching
set hlsearch			" Highlight search results
set smartcase			" When searching try to be smart about cases
set showmatch			" Show matching brackets when text indicator is over them
set expandtab			" Use spaces instead of tabs
set smarttab			" Be smart when using tabs ;)
set shiftwidth=4		" 1 tab == 4 spaces
set tabstop=4
set ai 					"Auto indent
set si 					"Smart indent
set wrap 				"Wrap lines
set completeopt+=noselect

" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
      \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
      endif

" vim plugins
call plug#begin()

Plug 'preservim/nerdtree', { 'on': 'NERDTreeToggle' } " NerdTree
Plug 'ryanoasis/vim-devicons' " Developer Icons
Plug 'vimwiki/vimwiki' " Notes Taking
Plug 'jiangmiao/auto-pairs' " Auto close pairs
Plug 'mattn/emmet-vim' " emmet
Plug 'francoiscabrol/ranger.vim' " Rnager in vim
Plug 'junegunn/fzf' " Fuzzy finder
Plug 'tpope/vim-commentary' " Comment
Plug 'lifepillar/vim-mucomplete' " Auto complession

call plug#end()

" Return to last edit position when opening files (You want this!)
au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif


