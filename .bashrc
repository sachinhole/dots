#
# ~/.bashrc
#

BASH_THEMES_DIR=~/.config/shell/bash/themes
BASH_PLUGINS_DIR=~/.config/shell/bash/plugins

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'

# bash theme
BASH_THEME="bash"

# bash plugins 
BASH_PLUGINS=( git )

# BASH_ENGINE
#source ~/.config/shell/bash/bashEngine

# BAHS CONF
[[ -f ~/.config/shell/alis ]] && source ~/.config/shell/alis

# starship Prompt
eval "$(starship init bash)"

