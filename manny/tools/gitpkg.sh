#!/usr/bin/env bash

source $HOME/.local/dots/manny/manny.conf

## simple helper to search, install, and update AUR packages 
baph(){
	git clone --depth=1 https://gitlab.com/sachinhole/baph.git ~/.local/src/baph
	sudo make -C ~/.local/src/baph install
	cd -	
}

## bash promt theme, and etc.
ohmybash(){
	 curl -s https://raw.githubusercontent.com/ohmybash/oh-my-bash/master/tools/install.sh -o /tmp/install.sh
	 sed -i 's/OSH=~\/.oh-my-bash/OSH=~\/.config\/shell\/bash\/oh-my-bash/g' /tmp/install.sh
	 bash /tmp/install.sh
}

# starship prompt
starship(){
	curl -sS https://starship.rs/install.sh | sh
	[[ -z $(grep "eval \"\$(starship init bash)\"" ~/.bashrc) ]] && echo -e "# starship prompt\neval \"\$(starship init bash)\""
}

## simple terminal (suckless)
st(){ 
	echo " work in progress!"
}
baph
# install packages
  echo $(awk '/^ \[G\]/ {print $2}' $ARCH_PKG) | while read -r PKG; do
	[[ -f /usr/bin/$PKG ]] || [[ -f /bin/$PKG ]] && continue 
	$PKG
  done


