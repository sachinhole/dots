#!/usr/bin/env bash

 source $HOME/.local/dots/manny/manny.conf

 [[ -d $DOTFILE_DIR ]] || mkdir -p $DOTFILE_DIR

 ADD(){
	awk '/^\[F\]/ {print $2" "$3}' $DOT_FILE | while read -r FILE FILE_PATH; do
		[[ -z $(find $FILE_PATH/$FILE -type l) ]] && mv -f $FILE_PATH/$FILE $DOTFILE_DIR && echo "     $DOTFILE_DIR/$FILE --> $FILE_PATH/$FILE " || continue
		ln -sf $DOTFILE_DIR/$FILE $FILE_PATH	
	done
	awk '/^\[D\]/ {print $2" "$3}' $DOT_FILE | while read -r DIR DIR_PATH; do
                 [[ -z $(find $DIR_PATH/$DIR -type l) ]] && mv -f $DIR_PATH/$DIR $DOTFILE_DIR && echo "         $DOTFILE_DIR/$DIR --> $DIR_PATH/$DIR " || continue
                 ln -sf $DOTFILE_DIR/$DIR $DIR_PATH
        done
 }

 SETUP(){
	awk '/^\[F\]/ {print $2" "$3}' $DOT_FILE | while read -r FILE FILE_PATH; do 
                [[ -z $(find $FILE_PATH/$FILE -type l) ]] && rm -rf $FILE_PATH/$FILE && ln -sf $DOTFILE_DIR/$FILE $FILE_PATH && echo " $DOTFILE_DIR/$FILE --> $FILE_PATH/$FILE "
        done
        awk '/^\[D\]/ {print $2" "$3}' $DOT_FILE | while read -r DIR DIR_PATH; do
                [[ -z $(find $DIR_PATH/$DIR   -type l) ]] && rm -rf $DIR_PATH/$DIR   && ln -sf $DOTFILE_DIR/$DIR $DIR_PATH   && echo " $DOTFILE_DIR/$DIR  --> $DIR_PATH/$DIR "

        done
 }
$1

